function showMessage(spanId, message) {
  document.getElementById(spanId).innerText = message;
}
function kiemTraTrung(id, dssv) {
  var index = dssv.findIndex(function (item) {
    return item.maSv == id;
  });
  if (index == -1) {
    showMessage("spanMaSV", "");
    return true;
  } else {
    showMessage("spanMaSV", "bị lỗi rồi  !!");
    return false;
  }
}

function kiemTraDoDai(min, max, idSpan, message, value) {
    var length = value.length;
    if (length >= min && length <= max) {
    showMessage (idSpan, "");
    return true;
    } else {
    showMessage (idSpan, message);
    return false
    }}