var dssv = [];
// ở file này chúng ta chỉ làm nhiệm vụ khai báo đối tượng và dom tới class và lấy giá trị sau đó gọi tới controller để in tất cả những gì lấy được ra màn hình
// lấy dữ liệu từ dưới localStorage và kiểm tra xem có dữ liệu hay không nếu như có dữ liệu thì sẽ chuyển đổi dữ liệu từ kiểu Json sang dạng array rồi từ đó render ra màn hình
// biến dataJson là biến được ghi đè luu dữ liệu DSSV
jsonData = localStorage.getItem("danhSachSinhVien");
if (jsonData != null) {
  // dssv được gán bằng giá trị sau chuyển đổi
  dssv = JSON.parse(jsonData).map(function (item) {
    return new SinhVien(
      item.maSv,
      item.tenSv,
      item.Email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
  });
  // map
  renderDSSV(dssv);
}

function themSinhVien() {
  // đưa dữ liệu vừa nhập vào mảng
var sv=layThongTinTuForm();
// validate
var isvalid =kiemTraTrung(sv.maSv,dssv) && 
kiemTraDoDai(5,5,"spanMaSV","Mã sinh viên phải từ 5 ký tự",sv.maSv);
//  "&": là phép cộng bit true+false=false 
 isvalid= isvalid & kiemTraDoDai(4,8,"spanMatKhau","Mật khẩu phải từ 4-8 ký tự mà bà nọi",sv.matKhau);

if(isvalid){

  dssv.push(sv);
  // hiển thị dữ liệu trong mảng
  renderDSSV(dssv);
  // chuyển đổi kiểu dữ liệu sang json và được lưu giá trị sau chuyển đổi vào dataJson
  var jsonData = JSON.stringify(dssv);
  // lưu dữ liệu vừa được chuyển đổi xuống localStorage với tên là danhSachSinhVien và giá trị là jsonData  kiểu dữ liệu là json
  localStorage.setItem("danhSachSinhVien", jsonData);
  // local storage chỉ có thể lưu dữ liệu dưới dạng Json
}}


function xoaSinhVien(maSv) {
  // for(var i=0 ; i<dssv.length;i++){
  //     if(maSv == dssv[i].maSv){
  //    dssv.splice(i,1);
  // }}
  var index = dssv.findIndex(function (item) {
    return (maSv==item.maSv );
  });
  dssv.splice(index, 1);
  renderDSSV(dssv);
    // chuyển đổi kiểu dữ liệu sang json và được lưu giá trị sau chuyển đổi vào dataJson
    var jsonData = JSON.stringify(dssv);
    // lưu dữ liệu vừa được chuyển đổi xuống localStorage với tên là danhSachSinhVien và giá trị là jsonData  kiểu dữ liệu là json
    localStorage.setItem("danhSachSinhVien", jsonData);
    // local storage chỉ có thể lưu dữ liệu dưới dạng Json
}


function suaSinhVien(id) {
 // tim vi trí sv trong dssv có má trung voi id cua onlick
    var index = dssv.findIndex(function
        (item) {
            return item.ma == id;
        });
        console.log('index:', dssv[item].tenSv);
    }
function capNhatSinhVien(){

}


function introduce(value, callback) {
  console.log(value);
  callback(callback);
  // sayHello ("alice")
}




// array map js w3
// call back function
// minh
function sayHello(name) {
  console.log("heelo", name);
}
introduce("alice", function (name) {
  console.log("bai bai bai");
});

function Dog(ten, tuoi) {
  this.nickname = ten;
  this.age = tuoi;
  this.speak = function () {
    console.log("cuu cuu cuu");
  };
}
var dog1 = new Dog("lulu", 2);
var dog2 = new Dog("mimi", 3);
console.log(dog1.speak, dog2);

