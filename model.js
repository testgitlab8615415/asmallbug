function SinhVien(maSv, tenSv, Email, matKhau, toan, ly, hoa) {
  this.maSv = maSv;
  this.tenSv = tenSv;
  this.Email = Email;
  this.matKhau = matKhau;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;
  this.tinhDTB = function () {
    return (this.toan + this.ly + this.hoa) / 3;
  };
}
